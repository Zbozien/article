package pl.szymo.article.controller;

import pl.szymo.article.model.Discovery;
import pl.szymo.article.model.User;
import pl.szymo.article.model.Vote;
import pl.szymo.article.model.VoteType;
import pl.szymo.article.service.DiscoveryService;
import pl.szymo.article.service.VoteService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//Głosy będą oddawane przez wysłanie uwierzytelnionego użytkownika np. na adres /vote?discovery_id=3&vote=VOTE_UP co oznacza oddanie głosu "na tak" na treść o id 3.
@WebServlet("/vote") // ten adres musze dodac do sekcji zabezpieczen w web.xml
public class VoteController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // sprawdzenie czy user jest zalogowany
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User loggedUser = (User) req.getSession().getAttribute("user");
        if (loggedUser != null) {
            VoteType voteType = VoteType.valueOf(req.getParameter("vote"));
            long userId = loggedUser.getId();
            long discoveryId = Long.parseLong(req.getParameter("discovery_id"));
            updateVote(userId, discoveryId, voteType);
        }
        // po oddaniu glosu, przekierowanie na główną, czyli automatycznie strona się odswiezy
        resp.sendRedirect(req.getContextPath() + "/");
    }

     // aktualizacja lub utworzenie glosu gdy go nie ma w tabeli vote, oraz aktualizacja vote_up i down w tabeli discovery
    private void updateVote(long userId, long discoveryId, VoteType voteType) {
        VoteService voteService = new VoteService();
        Vote existingVote = voteService.getVoteByDiscoveryUserId(discoveryId, userId);
        Vote updatedVote = voteService.addOrUpdateVote(discoveryId, userId, voteType);
        if (existingVote != updatedVote || !updatedVote.equals(existingVote)) { // nie pamietam co mialem na mysli w 2 przypadku
            updateDiscovery(discoveryId, existingVote, updatedVote);
        }
    }

    //obsluga zwiekszenia i zmniejszenia glosu, ogolnie obsluga 2 przypadkow:
    // user jeszcze nie glosowal, ale wlasnie go chce oddac, zwiekszam glos w dol lub gore o 1
    // user juz na tresc wczesniej glosowal, ale chce zmienic zdanie np gdy glosowal na tak a teraz chce na nie to
    //zmniejszam licznik na nie a dodaje do glosow na tak
    private void updateDiscovery(long discoveryId, Vote oldVote, Vote newVote) {
        DiscoveryService discoveryService = new DiscoveryService();
        Discovery discoveryById = discoveryService.getDiscoveryById(discoveryId);
        Discovery updatedDiscovery = null;
        if (oldVote == null && newVote != null) {
            updatedDiscovery = addDiscoveryVote(discoveryById, newVote.getVoteType());
        } else if (oldVote != null && newVote != null) {
            updatedDiscovery = removeDiscoveryVote(discoveryById, oldVote.getVoteType()); // delete old vote
            updatedDiscovery = addDiscoveryVote(updatedDiscovery, newVote.getVoteType()); // add new
        }
        discoveryService.updateDiscovery(updatedDiscovery);
    }

    //zwiekszenie glosu na tak i nie
    private Discovery addDiscoveryVote(Discovery discovery, VoteType voteType) {
        Discovery discoveryCopy = new Discovery(discovery);
        if (voteType == VoteType.VOTE_UP) {
            discoveryCopy.setUpVote(discoveryCopy.getUpVote() + 1);
        } else if (voteType == VoteType.VOTE_DOWN) {
            discoveryCopy.setDownVote(discoveryCopy.getDownVote() + 1);
        }
        return discoveryCopy;
    }

    //zmniejszenie glosu na tak i nie
    private Discovery removeDiscoveryVote(Discovery discovery, VoteType voteType) {
        Discovery discoveryCopy = new Discovery(discovery);
        if (voteType == VoteType.VOTE_UP) {
            discoveryCopy.setUpVote(discoveryCopy.getUpVote() - 1);
        } else if (voteType == VoteType.VOTE_DOWN) {
            discoveryCopy.setDownVote(discoveryCopy.getDownVote() - 1);
        }
        return discoveryCopy;
    }

}

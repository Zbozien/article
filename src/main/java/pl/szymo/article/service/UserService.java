package pl.szymo.article.service;

import pl.szymo.article.dao.DAOFactory;
import pl.szymo.article.dao.UserDAO;
import pl.szymo.article.model.User;

public class UserService {

    //    Na chwilę obecną konto użytkownika będzie domyślnie aktywowane, potem pomysle nad linkiem aktywacyjnym itp
    public void addUser(String username, String email, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setActive(true);
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDAO = factory.getUserDAO();
        userDAO.create(user);
    }

    public User getUserById(long userId) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDAO = factory.getUserDAO();
        User user = userDAO.read(userId);
        return user;
    }

    public User getUserByUsername(String username) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDAO = factory.getUserDAO();
        User user = userDAO.getUserByUsername(username);
        return user;
    }
}

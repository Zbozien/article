package pl.szymo.article.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, PK extends Serializable> {

    //CRUD dziedziczone w interfejsach DAO
    T create(T newObject);

    T read(PK primaryKey);

    boolean update(T updateObject);

    boolean delete(PK key);

    //The method returns all results from the given table
    List<T> getAll();
}

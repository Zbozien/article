package pl.szymo.article.dao;

import pl.szymo.article.model.User;

import java.util.List;

public interface UserDAO extends GenericDAO<User, Long> {

    List<User> getAll();

    User getUserByUsername(String username);

}

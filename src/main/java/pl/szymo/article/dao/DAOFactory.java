package pl.szymo.article.dao;

import pl.szymo.article.exception.NoSuchDbTypeException;
// tu moge zmieniajac implementacje metody grtDAOFactory przepiąc aplikacje na inny silnik bazy danych
public abstract class DAOFactory {

    public static final int MYSQL_DAO_FACTORY = 1;

    public abstract DiscoveryDAO getDiscoveryDAO();

    public abstract UserDAO getUserDAO();

    public abstract VoteDAO getVoteDAO();

    public static DAOFactory getDAOFactory() {
        DAOFactory factory = null;
        try {
            factory = getDAOFactory(MYSQL_DAO_FACTORY);
        } catch (NoSuchDbTypeException e) {
            e.printStackTrace();
        }
        return factory;
    }

    private static DAOFactory getDAOFactory(int type) throws NoSuchDbTypeException { //wyjątek informujący o podaniu niepoprawnego rodzaju bazy danych
        switch (type) {
            case MYSQL_DAO_FACTORY:
                return new MysqlDAOFactory();
            default:
                throw new NoSuchDbTypeException();
        }
    }
}

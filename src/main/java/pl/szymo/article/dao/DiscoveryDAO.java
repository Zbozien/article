package pl.szymo.article.dao;

import pl.szymo.article.model.Discovery;

import java.util.List;

public interface DiscoveryDAO extends GenericDAO<Discovery, Long> {

    List<Discovery> getAll();

}

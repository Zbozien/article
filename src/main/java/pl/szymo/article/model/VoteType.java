package pl.szymo.article.model;

public enum VoteType {
    VOTE_UP, VOTE_DOWN;
}
